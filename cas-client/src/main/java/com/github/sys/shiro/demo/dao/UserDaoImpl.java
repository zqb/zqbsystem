package com.github.sys.shiro.demo.dao;

import com.github.sys.shiro.demo.entity.Role;
import com.github.sys.shiro.demo.entity.User;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>User: 
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public class UserDaoImpl extends JdbcDaoSupport implements UserDao {

    public User createUser(final User user) {
        final String sql = "insert into ly_user(username, password, salt, locked) values(?,?,?, ?)";

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement psst = connection.prepareStatement(sql, new String[]{"id"});
                psst.setString(1, user.getUsername());
                psst.setString(2, user.getPassword());
                psst.setString(3, user.getSalt());
                psst.setBoolean(4, user.getLocked());
                return psst;
            }
        }, keyHolder);

        user.setId(keyHolder.getKey().longValue());
        return user;
    }

    public void updateUser(User user) {
        String sql = "update ly_user set username=?, password=?, salt=?, locked=? where id=?";
        getJdbcTemplate().update(sql, user.getUsername(), user.getPassword(), user.getSalt(), user.getLocked(), user.getId());
    }

    public void deleteUser(Long userId) {
        String sql = "delete from ly_user where id=?";
        getJdbcTemplate().update(sql, userId);
    }

    @Override
    public void correlationRoles(Long userId, Long... roleIds) {
        if(roleIds == null || roleIds.length == 0) {
            return;
        }
        String sql = "insert into ly_user_role(user_id, role_id) values(?,?)";
        for(Long roleId : roleIds) {
            if(!exists(userId, roleId)) {
                getJdbcTemplate().update(sql, userId, roleId);
            }
        }
    }

    @Override
    public void uncorrelationRoles(Long userId, Long... roleIds) {
        if(roleIds == null || roleIds.length == 0) {
            return;
        }
        String sql = "delete from ly_user_role where user_id=? and role_id=?";
        for(Long roleId : roleIds) {
            if(exists(userId, roleId)) {
                getJdbcTemplate().update(sql, userId, roleId);
            }
        }
    }

    private boolean exists(Long userId, Long roleId) {
        String sql = "select count(1) from ly_user_role where user_id=? and role_id=?";
        return getJdbcTemplate().queryForObject(sql, Integer.class, userId, roleId) != 0;
    }


    @Override
    public User findOne(Long userId) {
        String sql = "select id, usernames,username, password, salt, locked from ly_user where id=?";
        List<User> userList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper(User.class), userId);
        if(userList.size() == 0) {
            return null;
        }
        return userList.get(0);
    }

    @Override
    public User findByUsername(String username) {
        String sql = "select id,usernames, username, password, salt, locked from ly_user where username=?";
        List<User> userList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper(User.class), username);
        if(userList.size() == 0) {
            return null;
        }
        return userList.get(0);
    }
    
	public Set<String> getRolesAsString(Integer userId)  {
		List<Role> list = null;
		try {
			list = findRoleByUserId(userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		Set<String> set = new HashSet<String>();
		for (Role r : list) {
//			set.add(r.getCode());
			set.add(r.getName());
		}
		return set;
	}
	
	public List<Role> findRoleByUserId(Integer userId) throws Exception {
		if (userId == null) {
			return null;
		}
//		String sql="SELECT r.* from ly_role r,ly_user_role  u_re,ly_user u where u.id=? and u_re.roleId=r.id and u_re.userId=u.id";
//		 return getJdbcTemplate().queryForList(sql, Role.class, userid);
		String sql = "SELECT r.* from ly_role r,ly_user_role  u_re,ly_user u where u.id=? and u_re.roleId=r.id and u_re.userId=u.id";
        List<Role> roleList = getJdbcTemplate().query(sql, new BeanPropertyRowMapper(Role.class), userId);
        if(roleList.size() == 0) {
            return null;
        }
        return roleList;
	}

//    @Override
//    public Set<String> findRoles(String username) {
//        String sql = "select role from ly_user u, ly_role r,ly_user_role ur where u.username=? and u.id=ur.userId and r.id=ur.roleId";
//        return new HashSet(getJdbcTemplate().queryForList(sql, String.class, username));
//    }
    
    

    @Override
    public Set<String> findPermissions(String username) {
        //TODO 此处可以优化，比如查询到role后，一起获取roleId，然后直接根据roleId获取即可
        String sql = "select res.resUrl from ly_user u ,ly_resources res,ly_res_user res_u where " +
        		"u.username=? and u.id=res_u.userId  and  res.id=res_u.resId";
        return new HashSet(getJdbcTemplate().queryForList(sql, String.class, username));
    }

	@Override
	public Set<String> findRoles(String username) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
