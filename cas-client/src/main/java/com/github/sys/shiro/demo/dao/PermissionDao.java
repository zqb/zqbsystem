package com.github.sys.shiro.demo.dao;

import com.github.sys.shiro.demo.entity.Permission;

/**
 * <p>User: 
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public interface PermissionDao {

    public Permission createPermission(Permission permission);

    public void deletePermission(Long permissionId);

}
