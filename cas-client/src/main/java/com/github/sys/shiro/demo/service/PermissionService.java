package com.github.sys.shiro.demo.service;

import com.github.sys.shiro.demo.entity.Permission;

/**
 * <p>User: 
 * <p>Date: 14-1-28
 * <p>Version: 1.0
 */
public interface PermissionService {
    public Permission createPermission(Permission permission);
    public void deletePermission(Long permissionId);
}
