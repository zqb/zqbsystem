package com.skycomm.cpip.controller.system;

import javax.inject.Inject;

import com.skycomm.cpip.controller.index.BaseController;
import com.skycomm.cpip.entity.LogFormMap;
import com.skycomm.cpip.mapper.LogMapper;
import com.skycomm.cpip.util.Common;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skycomm.cpip.plugin.PageView;

/**
 * 
 * @author zqb 2014-11-19
 * @Email: 
 * @version 1.0v
 */
@Controller
@RequestMapping("/log/")
public class LogController extends BaseController {
	@Inject
	private LogMapper logMapper;

	@RequestMapping("list")
	public String listUI(Model model) throws Exception {
		return Common.BACKGROUND_PATH + "/system/log/list";
	}

	@ResponseBody
	@RequestMapping("findByPage")
	public PageView findByPage( String pageNow,
			String pageSize) throws Exception {
		LogFormMap logFormMap = getFormMap(LogFormMap.class);
		String order = " order by id asc";
		logFormMap.put("$orderby", order);
		logFormMap=toFormMap(logFormMap, pageNow, pageSize);
        pageView.setRecords(logMapper.findByPage(logFormMap));
		return pageView;
	}
}