package com.skycomm.cpip.entity;

import com.skycomm.cpip.annotation.TableSeg;
import com.skycomm.cpip.util.FormMap;

@TableSeg(tableName = "dept", id="id")
public class DeptFormMap  extends FormMap<String,Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
