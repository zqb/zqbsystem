package com.skycomm.cpip.mapper;

import com.skycomm.cpip.entity.ResFormMap;
import com.skycomm.cpip.mapper.base.BaseMapper;

import java.util.List;

public interface ResourcesMapper extends BaseMapper {
	public List<ResFormMap> findChildlists(ResFormMap map);

	public List<ResFormMap> findRes(ResFormMap map);

	public void updateSortOrder(List<ResFormMap> map);
	
	public List<ResFormMap> findUserResourcess(String userId);
	
}
