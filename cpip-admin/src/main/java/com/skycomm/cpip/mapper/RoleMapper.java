package com.skycomm.cpip.mapper;

import java.util.List;

import com.skycomm.cpip.entity.RoleFormMap;
import com.skycomm.cpip.mapper.base.BaseMapper;

public interface RoleMapper extends BaseMapper{
	public List<RoleFormMap> seletUserRole(RoleFormMap map);
	
	public void deleteById(RoleFormMap map);
}
