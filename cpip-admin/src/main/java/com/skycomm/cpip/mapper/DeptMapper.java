package com.skycomm.cpip.mapper;

import java.util.List;

import com.skycomm.cpip.entity.DeptFormMap;
import com.skycomm.cpip.mapper.base.BaseMapper;

public interface DeptMapper extends BaseMapper {
	public List<DeptFormMap> findChildlists(DeptFormMap map);

	public List<DeptFormMap> findRes(DeptFormMap map);

	
}
